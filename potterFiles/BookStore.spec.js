const BookStore = require('./BookStore')

describe('Potter Kata', () => {
  it('charges an specified ammount after selling one book', () => {
    const bookStore = new BookStore()
    const book = { 'Stone': 1 }
    const bookPrice = 8
    
    const purchase = bookStore.sell(book)

    expect(purchase).toBe(bookPrice)
  })

  it('gives a 5% discount if two DIFFERENT books are bought', () => {
    const bookStore = new BookStore()
    const books = {'Stone':1, 'Chamber':1}
    const bookPrice = 8 * 2
    const discount = 0.95
   
    const purchase = bookStore.sell(books)

    expect(purchase).toBe(bookPrice * discount)
  })

  it('gives a 10% discount if three DIFFERENT books are bought', () => {
    const bookStore = new BookStore()
    const books = {'Stone':1, 'Chamber':1, 'Prisoner':1}
    const bookPrice = 8 * 3
    const discount = 0.90
   
    const purchase = bookStore.sell(books)

    expect(purchase).toBe(bookPrice * discount)
  })
})