const BOOK_PRICE = 8
const discount = {
  1:1,
  2:0.95,
  3:0.90,
  4: 0.80,
  5: 0.75
}

class BookStore{
  // constructor no funciona???

  sell(books){
   const numberOfBooks = Object.keys(books).length
   const discountGranted = discount[numberOfBooks]

   return numberOfBooks * BOOK_PRICE * discountGranted
  }
}

module.exports = BookStore